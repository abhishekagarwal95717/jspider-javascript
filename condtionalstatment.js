//control statment
//if
/*var a=2;
var b=5;

if(b>a)
{
    console.log("b is greater");

}*/

//if else statment
/*
var a=2;
var b=3;

if(a>b)
{
    console.log("a is greater than");

}   
 else
 {
     console.log("b is greater than")
 }
*/

// even or odd number
  /*
var a=8;
 if(a%2==0)
 {
     console.log("even");

 }
 else
 {
     console.log("odd");
 }
 */

 /*
 var age=18;
  
 if(age>=18)
 {
     console.log("eligible to vote");

 }
 else
 {
     console.log("not eligible to vote");
 }
*/

 //ternary/conditinal operator
/*
 var num=8;
 (num%2==0)?console.log("even"): console.log("odd");;
*/

var salary=100000;
if(salary>=500000)
{
    console.log("pay tax 5%");
}
else
{
    console.log("no pay tax");
}
